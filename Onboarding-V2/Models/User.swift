//
//  User.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/9/21.
//

import Foundation

struct User {
    let email: String
    let fullname: String
    var hasSeenOnboarding: Bool = false
    let uid: String
}

extension User {
    init(uid: String, dictionary: jsonDictionary){
        self.uid = uid
        self.email = dictionary["email"] as? String ?? ""
        self.fullname = dictionary["fullname"] as? String ?? ""
        self.hasSeenOnboarding = dictionary["hasSeenOnboarding"] as? Bool ?? false
    }
}
