//
//  NewUser.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/7/21.
//

import Foundation

struct NewUser: Encodable {
    var email: String
    var fullname: String
    var password: String? = nil
    var hasSeenOnboarding: Bool = false
}

extension NewUser {
    init?(_ newUser:NewUser){
        self.email = newUser.email
        self.fullname = newUser.fullname
    }
}
