//
//  ValidationAttributedButton.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/7/21.
//

import UIKit

class ValidationAttributedButton: UIButton {
    
    var titleWithQuestionMark: String? = nil {
        didSet {
            configure()
        }
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    private func configure(){
        let attributedString = convertTitleToAttributedText()
        setAttributedTitle(attributedString, for: .normal)
    }
    
    private func convertTitleToAttributedText() -> NSAttributedString? {
        
        guard let labels = titleWithQuestionMark?.components(separatedBy: "? "),
              labels.count == 2 else {
            return nil
        }
        
        let regularLabel = labels[0]
        let boldLabel = labels[1]
        
        let attributes: [NSAttributedString.Key:Any] = [.foregroundColor: UIColor(white: 1, alpha: 0.87), .font:UIFont.systemFont(ofSize:15)]
        
        let attributedTitle = NSMutableAttributedString(string: "\(regularLabel)?", attributes: attributes)
        
        let boldAttributes: [NSAttributedString.Key:Any] = [.foregroundColor: UIColor(white: 1, alpha: 0.87), .font: UIFont.boldSystemFont(ofSize: 15)]
        
        attributedTitle.append(NSAttributedString(string: " \(boldLabel)", attributes: boldAttributes))
        
        return attributedTitle
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
