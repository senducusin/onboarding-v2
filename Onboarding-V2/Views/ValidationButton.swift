//
//  ValidationButton.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/6/21.
//

import UIKit

class ValidationButton: UIButton {
    
    var title: String? = nil {
        didSet {
            setTitle(title, for: .normal)
            titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        }
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)

        layer.cornerRadius = 5
        backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1).withAlphaComponent(0.5)
        setHeight(height: 50)
        isEnabled = false
        
        setTitleColor(UIColor(white: 1, alpha: 0.67), for: .normal)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
