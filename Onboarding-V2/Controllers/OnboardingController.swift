//
//  OnboardingController.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/7/21.
//

import paper_onboarding

protocol OnboardingControllerDelegate: class {
    func controllerWillDismiss(_ controller: OnboardingController)
}

class OnboardingController: UIViewController {
    // MARK: - Properties
    private var onboardingItems = [OnboardingItemInfo]()
    private var onboardingView = PaperOnboarding()
    weak var delegate: OnboardingControllerDelegate?
    
    private var getStartedButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Get Started", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 24)
        button.addTarget(self, action: #selector(dismissOnboarding), for: .touchUpInside)
        button.alpha = 0
        return button
    }()
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        onboardingView.dataSource = self
        onboardingView.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    // MARK: - Selectors
    @objc private func dismissOnboarding(){
        delegate?.controllerWillDismiss(self)
    }
    
    // MARK: - Helpers
    private func setupUI(){
        view.addSubview(onboardingView)
        onboardingView.fillSuperview()
        
        view.addSubview(getStartedButton)
        getStartedButton.centerX(inView: view)
        getStartedButton.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor, paddingBottom: 128)
    }
    
    private func animateGetStartedButton(_ shouldShow: Bool){
        if shouldShow {
            UIView.animate(withDuration: 0.5){
                self.getStartedButton.alpha = 1
            }
        }
    }
}

extension OnboardingController: PaperOnboardingDataSource{
    func onboardingItemsCount() -> Int {
        return OnboardingViewModel.allCases.count
    }
    
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        let viewModel = OnboardingViewModel(rawValue: index)
        
        guard let image = viewModel?.image,
              let title = viewModel?.title,
              let description = viewModel?.description,
              let backgroundColor = viewModel?.backgroundColor else {
            
            return OnboardingItemInfo(
                informationImage:#imageLiteral(resourceName: "baseline_dashboard_white_48pt").withRenderingMode(.alwaysOriginal),
                title: "Error Page",
                description: "This is an invalid page",
                pageIcon: UIImage(),
                color: .systemPurple,
                titleColor: .white,
                descriptionColor: .white,
                titleFont: .boldSystemFont(ofSize: 24),
                descriptionFont: .systemFont(ofSize: 16)
            )
        }
        
        return OnboardingItemInfo(
            informationImage: image,
            title: title,
            description: description,
            pageIcon: UIImage(),
            color: backgroundColor,
            titleColor: .white,
            descriptionColor: .white,
            titleFont: .boldSystemFont(ofSize: 24),
            descriptionFont: .systemFont(ofSize: 16)
        )
    }
}
 
extension OnboardingController: PaperOnboardingDelegate {
    func onboardingDidTransitonToIndex(_ index: Int) {
        if let viewModel = OnboardingViewModel(rawValue: index) {
            animateGetStartedButton(viewModel.shouldShow)
        }
    }
}
