//
//  ResetPasswordController.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/6/21.
//

import UIKit

protocol ResetPasswordDelegate: class {
    func didSendResetPasswordLink()
}

class ResetPasswordController: UIViewController {
    // MARK: - Properties
    private var viewModel = ResetPasswordViewModel()
    var email: String?
    weak var delegate: ResetPasswordDelegate?
    
    private let iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "paperplane")
        imageView.tintColor = .systemYellow
        return imageView
    }()
    
    private let emailAddressTextField: ValidationTextfield = {
        let textField = ValidationTextfield(placeholder: "Email Address")
        textField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        return textField
    }()
    
    private let resetPasswordButton: ValidationButton = {
        let button = ValidationButton(type: .system)
        button.title = "Send Reset Link"
        button.addTarget(self, action: #selector(resetPasswordButtonHandler), for: .touchUpInside)
        return button
    }()
    
    private let backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "chevron.left"), for: .normal)
        button.addTarget(self, action: #selector(backButtonHandler), for: .touchUpInside)
        button.tintColor = .white
        return button
    }()
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadEmail()
    }
    
    // MARK: - Selectors
    @objc private func textDidChange(_ sender: UITextField){
        
        if sender == emailAddressTextField {
            viewModel.emailAddress = sender.text
        }
        
        updateForm()
    }
    
    @objc private func resetPasswordButtonHandler(){
        guard let email = viewModel.emailAddress else { return }
        showLoader(true)
        
        AuthService.shared.resetPassword(forEmail: email) { [weak self] error in
            self?.showLoader(false)
            if let error = error {
                self?.showMessage(withTitle: "Error", message: error.localizedDescription)
                return
            }
            self?.navigationController?.popViewController(animated: true)
            self?.delegate?.didSendResetPasswordLink()
        }
    }
    
    @objc private func backButtonHandler(){
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Helpers
    private func loadEmail(){
        guard let email = email else { return }
        viewModel.emailAddress = email
        emailAddressTextField.text = email
        updateForm()
    }
    
    private func updateForm(){
        resetPasswordButton.isEnabled = viewModel.formIsValid
        resetPasswordButton.setTitleColor(viewModel.buttonTitleColor, for: .normal)
        resetPasswordButton.backgroundColor = viewModel.buttonBackgroundColor
    }
    
    private func setupUI(){
        view.setGradientWithTheme()
        setupBackButton()
        setupIcon()
        setupStackView()
    }
    
    private func setupBackButton(){
        view.addSubview(backButton)
        backButton.anchor(top:view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, paddingTop: 16, paddingLeft: 16)
    }
    
    private func setupStackView(){
        let stack = UIStackView(arrangedSubviews: [
            emailAddressTextField,
            resetPasswordButton
        ])
        stack.axis = .vertical
        stack.spacing = 20
        view.addSubview(stack)
        stack.anchor(top: iconImage.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 32, paddingLeft: 32, paddingRight: 32)
    }
    
    private func setupIcon(){
        view.addSubview(iconImage)
        iconImage.centerX(inView: view)
        iconImage.setDimensions(height: 120, width: 120)
        iconImage.anchor(top: view.safeAreaLayoutGuide.topAnchor, paddingTop: 32)
    }
}
