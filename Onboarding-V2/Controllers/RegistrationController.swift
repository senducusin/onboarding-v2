//
//  RegistrationController.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/6/21.
//

import UIKit

class RegistrationController: UIViewController {
    // MARK: - Properties
    
    private var viewModel = RegistrationViewModel()
    
    weak var delegate: AuthenticationDelegate?
    
    private let iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "paperplane")
        imageView.tintColor = .systemYellow
        return imageView
    }()
    
    private let emailTextField: ValidationTextfield = {
        let textField = ValidationTextfield(placeholder: "Email")
        textField.addTarget(self, action: #selector(textDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    private let fullNameTextField: ValidationTextfield = {
        let textField = ValidationTextfield(placeholder: "Full name")
        textField.addTarget(self, action: #selector(textDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    private let passwordTextField: ValidationTextfield = {
        let textField = ValidationTextfield(placeholder: "Password")
        textField.addTarget(self, action: #selector(textDidChange(_:)), for: .editingChanged)
        return textField
    }()
    
    private let signUpButton: ValidationButton = {
        let button = ValidationButton(type: .system)
        button.title = "Sign Up"
        button.addTarget(self, action: #selector(handleSignUpButton), for: .touchUpInside)
        return button
    }()
    
    private let alreadyHaveAnAccountButton: ValidationAttributedButton = {
        let button = ValidationAttributedButton(type: .system)
        button.titleWithQuestionMark = "Already have an account? Log in"
        
        button.addTarget(self, action: #selector(alreadyHaveAnAccountButtonHandler), for: .touchUpInside)
        return button
    }()
    
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: - Selectors
    @objc private func textDidChange(_ sender: UITextField){
        switch sender {
        case emailTextField:
            viewModel.email = sender.text
            
        case passwordTextField:
            viewModel.password = sender.text
            
        case fullNameTextField:
            viewModel.fullName = sender.text
            
        default:
            break
        }
        
        updateForm()
    }
    
    @objc private func handleSignUpButton(){
        guard let email = emailTextField.text,
              let fullname = fullNameTextField.text,
              let password = passwordTextField.text else {
            return
        }
        
        showLoader(true)
        
        let newUser = NewUser(email: email, fullname: fullname, password: password)
        
        AuthService.shared.createNewUser(newUser) { [weak self] result in
            
            self?.showLoader(false)
            
            switch result {
            
            case .success(_):
                self?.delegate?.authenticationComplete()
            case .failure(let error):
                self?.showMessage(withTitle: "Error", message: error.localizedDescription)
            }
        }
    }
    
    @objc private func alreadyHaveAnAccountButtonHandler(){
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Helpers
    private func updateForm(){
        signUpButton.isEnabled = viewModel.formIsValid
        signUpButton.setTitleColor(viewModel.buttonTitleColor, for: .normal)
        signUpButton.backgroundColor = viewModel.buttonBackgroundColor
    }
    
    private func setupUI(){
        view.setGradientWithTheme()
        setupIcon()
        setupStackView()
        setupDontHaveAnAccountButton()
    }
    
    private func setupDontHaveAnAccountButton(){
        view.addSubview(alreadyHaveAnAccountButton)
        alreadyHaveAnAccountButton.centerX(inView: view)
        alreadyHaveAnAccountButton.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor)
    }
    
    private func setupStackView(){
        let stack = UIStackView(arrangedSubviews: [
            emailTextField,
            fullNameTextField,
            passwordTextField,
            signUpButton
        ])
        stack.axis = .vertical
        stack.spacing = 20
        view.addSubview(stack)
        stack.anchor(top: iconImage.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 32, paddingLeft: 32, paddingRight: 32)
    }
    
    private func setupIcon(){
        view.addSubview(iconImage)
        iconImage.centerX(inView: view)
        iconImage.setDimensions(height: 120, width: 120)
        iconImage.anchor(top: view.safeAreaLayoutGuide.topAnchor, paddingTop: 32)
    }
}
