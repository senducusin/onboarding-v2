//
//  LoginController.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/6/21.
//

import UIKit
import GoogleSignIn

protocol AuthenticationDelegate: class {
    func authenticationComplete()
}

class LoginController:UIViewController {
    // MARK: - Properties
    
    private var viewModel = LoginViewModel()
    
    weak var delegate: AuthenticationDelegate?
    
    private let iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "paperplane")
        imageView.tintColor = .systemYellow
        return imageView
    }()
    
    private let emailTextField: ValidationTextfield = {
        let textField = ValidationTextfield(placeholder: "Email")
        textField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        return textField
    }()
    
    private let passwordTextField: ValidationTextfield = {
        let textField = ValidationTextfield(placeholder: "Password")
        textField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        return textField
    }()
    
    private let loginButton: ValidationButton = {
        let button = ValidationButton(type: .system)
        button.title = "Log In"
        button.addTarget(self, action: #selector(loginButtonHandler), for: .touchUpInside)
        return button
    }()
    
    private let dividerView = DividerView()
    
    private let forgotPasswordButton: UIButton = {
        let button = ValidationAttributedButton(type: .system)
        button.titleWithQuestionMark = "Forgot your password? Get help signing in"
        
        button.addTarget(self, action: #selector(forgotPasswordButtonHandler), for: .touchUpInside)
        return button
    }()
    
    private let googleLoginButton: UIButton = {
        let button = UIButton(type:.system)
        button.setImage(#imageLiteral(resourceName: "btn_google_light_pressed_ios").withRenderingMode(.alwaysOriginal), for: .normal)
        button.setTitle(" Log in with Google", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 16)
        button.addTarget(self, action: #selector(googleLoginButtonHandler), for: .touchUpInside)
        return button
    }()
    
    private let dontHaveAccountButton: ValidationAttributedButton = {
        let button = ValidationAttributedButton(type: .system)
        button.titleWithQuestionMark = "Don't have an account? Sign Up"
        
        button.addTarget(self, action: #selector(dontHaveAccountButtonHandler), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupGoogleLoginButton()
    }
    
    // MARK: - Selectors
    @objc private func loginButtonHandler(){
        guard let email = emailTextField.text,
              let password = passwordTextField.text else {
            return
        }
        
        showLoader(true)
        
        AuthService.shared.signIn(email: email, password: password) { [weak self] _ , error  in
            self?.showLoader(false)
            if let error = error {
                self?.showMessage(withTitle: "Error", message: error.localizedDescription)
                
                return
            }
            self?.delegate?.authenticationComplete()
        }
    }
    
    @objc private func forgotPasswordButtonHandler(){
        let controller = ResetPasswordController()
        controller.email = emailTextField.text
        controller.delegate = self
        pushTo(viewController: controller)
    }
    
    @objc private func googleLoginButtonHandler(){
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @objc private func dontHaveAccountButtonHandler(){
        let controller = RegistrationController()
        controller.delegate = delegate
        pushTo(viewController: controller)
    }
    
    @objc private func textDidChange(_ sender: UITextField){
        if sender == emailTextField {
            viewModel.email = sender.text
        } else if sender == passwordTextField {
            viewModel.password = sender.text
        }
        updateForm()
    }
    
    // MARK: - Helpers
    private func updateForm(){
        loginButton.isEnabled = viewModel.formIsValid
        loginButton.setTitleColor(viewModel.buttonTitleColor, for: .normal)
        loginButton.backgroundColor = viewModel.buttonBackgroundColor
    }
    
    private func pushTo(viewController:UIViewController){
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    private func setupUI(){
        view.setGradientWithTheme()
        setupNavigationBar()
        setupIcon()
        setupStackView()
        setupDontHaveAnAccountButton()
    }
    
    private func setupDontHaveAnAccountButton(){
        view.addSubview(dontHaveAccountButton)
        dontHaveAccountButton.centerX(inView: view)
        dontHaveAccountButton.anchor(bottom: view.safeAreaLayoutGuide.bottomAnchor)
    }
    
    private func setupStackView(){
        let stack = UIStackView(arrangedSubviews: [
            emailTextField,
            passwordTextField,
            loginButton,
            forgotPasswordButton,
            dividerView,
            googleLoginButton
        ])
        stack.axis = .vertical
        stack.spacing = 20
        view.addSubview(stack)
        stack.anchor(top: iconImage.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, paddingTop: 32, paddingLeft: 32, paddingRight: 32)
    }
    
    private func setupIcon(){
        view.addSubview(iconImage)
        iconImage.centerX(inView: view)
        iconImage.setDimensions(height: 120, width: 120)
        iconImage.anchor(top: view.safeAreaLayoutGuide.topAnchor, paddingTop: 32)
    }
    
    private func setupNavigationBar(){
        navigationController?.navigationBar.isHidden = true
        navigationController?.navigationBar.barStyle = .black
    }
    
    private func setupGoogleLoginButton(){
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
    }
}

// MARK: - Delegates

extension LoginController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
            return
        }
        
        AuthService.shared.signIn(withGoogle: user) { [weak self] result in
            switch result{
            case .success(_):
                self?.dismiss(animated: true, completion: nil)
                self?.delegate?.authenticationComplete()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
}

extension LoginController: ResetPasswordDelegate {
    func didSendResetPasswordLink() {
        self.showMessage(withTitle: "Success", message: "A link to reset your password is sent to your email address.")
    }
}
