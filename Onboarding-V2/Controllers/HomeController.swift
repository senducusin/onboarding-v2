//
//  HomeController.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/7/21.
//

import UIKit

class HomeController: UIViewController {
    // MARK: - Properties
    private var user: User? {
        didSet {
            shouldPresentOnboardingController()
        }
    }
    
    private let welcomeLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = .systemFont(ofSize: 28)
        label.alpha = 0
        return label
    }()
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        authenticateUser()
        setupUI()
    }
    
    // MARK - Selectors
    @objc private func handleLogoutButton(){
        logoutAlertHandler()
    }
    
    // MARK: - API
    
    private func fetchUser(){
        AuthService.shared.fetchUser { [weak self] result in
            
            switch result {
            case .success(let user):
                self?.user = user
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    private func authenticateUser(){
        if AuthService.shared.isLoggedIn == false {
//            print("DEBUG: authenticate 0")
            DispatchQueue.main.async {
                self.showLogin(animated: false)
            }
        }else{
//            print("DEBUG: authenticate 1")
            fetchUser()
        }
    }
    
    private func logoutAlertHandler(){
        let alert = UIAlertController(title: nil, message: "Are you sure you want to log out?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Log Out", style: .destructive, handler: { _ in
            AuthService.shared.signout { [weak self] error in
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
                self?.welcomeLabel.alpha = 0
                self?.showLogin(animated: true)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Helpers
    private func shouldPresentOnboardingController(){
        guard let user = user else {return}
        print(user)
        guard !user.hasSeenOnboarding else {
            showWelcomeLabel()
            return
        }
        
        let controller = OnboardingController()
        controller.delegate = self
        controller.modalPresentationStyle = .fullScreen
        present(controller, animated: true, completion: nil)
    }
    
    private func showWelcomeLabel(){
        guard let user = user,
              user.hasSeenOnboarding,
              welcomeLabel.alpha != 1 else {return}
        
        welcomeLabel.text = "Welcome, \(user.fullname)"
        UIView.animate(withDuration: 0.5) {
            self.welcomeLabel.alpha = 1
        }
    }
    
    private func showLogin(animated:Bool){
        let controller = LoginController()
        controller.delegate = self
        let nav = UINavigationController(rootViewController: controller)
        nav.modalPresentationStyle = .fullScreen
        present(nav, animated: animated, completion: nil)
    }
    
    private func setupUI(){
        view.setGradientWithTheme()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.barStyle = .black
        navigationItem.title = "Firebase Login"
        
        let image = UIImage(systemName: "arrow.left")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handleLogoutButton))
        navigationItem.leftBarButtonItem?.tintColor = .white
        
        view.addSubview(welcomeLabel)
        welcomeLabel.centerX(inView: view)
        welcomeLabel.centerY(inView: view)
    }
}

extension HomeController: OnboardingControllerDelegate {
    func controllerWillDismiss(_ controller: OnboardingController) {
        AuthService.shared.updateUserHasSeenOnboarding { [weak self] _ in
            controller.dismiss(animated: true, completion: nil)
            self?.showWelcomeLabel()
            self?.user?.hasSeenOnboarding = true
        }
    }
}

extension HomeController: AuthenticationDelegate {
    func authenticationComplete() {
        dismiss(animated: true, completion: nil)
        fetchUser()
    }
}
