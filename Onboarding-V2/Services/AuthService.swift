//
//  AuthService.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/7/21.
//

import Foundation
import Firebase
import GoogleSignIn

enum AuthServiceError:Error {
    case errorInCreatingUidAndDictionary
    case errorPasswordNotDefined
    case errorAuthenticatingCredentials
    case errorCreatingNewUser
    case errorUserNotFound
}

typealias DatabaseCompletion = ((Result<DatabaseReference?,Error>) -> Void)

class AuthService {
    
    private let usersReference = Database.database().reference().child("users")
    
    private func userExist(uid:String, completion:@escaping((User?)->())){
        usersReference.child(uid).observeSingleEvent(of: .value) { snapshot in
            if !snapshot.exists(){
                completion(nil)
            }else{
                guard let userDictionary = snapshot.value as? jsonDictionary else {
                    completion(nil)
                    return
                }
                
                let user = User(uid: uid, dictionary: userDictionary)
                completion(user)
            }
        }
    }
}

extension AuthService {
    
    static let shared = AuthService()
    
    var isLoggedIn: Bool {
        return Auth.auth().currentUser?.uid == nil ? false : true
    }
    
    public func resetPassword(forEmail email:String, completion:@escaping(Error?)->()){
        Auth.auth().sendPasswordReset(withEmail: email, completion: completion)
    }
    
    public func createNewUser(_ user: NewUser, completion: @escaping(DatabaseCompletion)){
        guard let password = user.password else {
            completion(.failure(AuthServiceError.errorPasswordNotDefined))
            return
        }
        
        Auth.auth().createUser(withEmail: user.email, password: password) { [weak self] result, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            let user = NewUser(user)
            
            guard let uid = result?.user.uid,
                  let userDictionary = user.toDictionary() else {
                completion(.failure(AuthServiceError.errorInCreatingUidAndDictionary))
                return
            }
            
            self?.usersReference.child(uid).updateChildValues(userDictionary) { error, reference in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                completion(.success(reference))
            }
        }
    }
    
    public func signIn(email:String, password:String, completion:AuthDataResultCallback?){
        Auth.auth().signIn(withEmail: email, password: password ,completion: completion)
    }
    
    public func signIn(withGoogle user: GIDGoogleUser, completion:@escaping(DatabaseCompletion)){
        
        guard let authentication = user.authentication else {
            completion(.failure(AuthServiceError.errorAuthenticatingCredentials))
            return
        }
        
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        
        Auth.auth().signIn(with: credential) { [weak self] result, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let uid = result?.user.uid,
                  let email = result?.user.email,
                  let fullname = result?.user.displayName else {
                completion(.failure(AuthServiceError.errorCreatingNewUser))
                return
            }
            
            self?.userExist(uid: uid, completion: { user in
                if user == nil {
                    guard let userDictionary = NewUser(email: email, fullname: fullname).toDictionary() else {
                        return
                    }
                    
                    self?.usersReference.child(uid).updateChildValues(userDictionary) { error, reference in
                        if let error = error {
                            completion(.failure(error))
                            return
                        }
                        completion(.success(reference))
                    }
                }else{
                    completion(.success(nil))
                }
            })
            
        }
    }
    
    public func signout(completion:@escaping(Error?) -> ()){
        do {
            try Auth.auth().signOut()
            completion(nil)
        } catch {
            completion(error)
        }
    }
    
    public func fetchUser(completion: @escaping(Result<User,AuthServiceError>)->()){
        guard let uid = Auth.auth().currentUser?.uid else {return}
        usersReference.child(uid).observeSingleEvent(of: .value) { snapshot in
            
            guard let userDictionary = snapshot.value as? jsonDictionary else {
                completion(.failure(.errorUserNotFound))
                return
            }
            
            let user = User(uid: uid, dictionary: userDictionary)
            completion(.success(user))
        }
    }
    
    public func updateUserHasSeenOnboarding(completion:@escaping(DatabaseCompletion)){
        guard let uid = Auth.auth().currentUser?.uid else {return}
        
        usersReference.child(uid).child("hasSeenOnboarding").setValue(true) { error, ref in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            completion(.success(ref))
            
        }
    }
}
