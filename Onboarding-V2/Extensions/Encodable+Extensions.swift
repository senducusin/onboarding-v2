//
//  Encodable+Extensions.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/7/21.
//

import Foundation

typealias jsonDictionary = [String: Any]

extension Encodable {
    func toDictionary() -> jsonDictionary? {
        if let data = try? JSONEncoder().encode(self) {
            if let dictionary = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? jsonDictionary {
                return dictionary
            }
        }
        
        return nil
    }
}
