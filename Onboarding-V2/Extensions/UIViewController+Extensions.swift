//
//  UIViewController+Extensions.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/9/21.
//

import UIKit
import JGProgressHUD

extension UIViewController {
    static let hud = JGProgressHUD(style: .dark)
    
    func showLoader(_ show: Bool){
        view.endEditing(true)
        
        if show {
            UIViewController.hud.show(in: view)
        }else{
            UIViewController.hud.dismiss()
        }
    }
    
    func showMessage(withTitle title:String, message:String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }
}
