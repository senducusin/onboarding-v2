//
//  LoginViewModel.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/7/21.
//

import UIKit

struct LoginViewModel {
    var email: String?
    var password: String?
    
    private var _formIsValid: Bool {
        return email?.isEmpty == false &&
            password?.isEmpty == false
    }
    
    var formIsValid: Bool {
        return _formIsValid
    }
    
    var buttonTitleColor: UIColor {
        return _formIsValid ? .white : UIColor(white: 1, alpha: 0.67)
    }
    
    var buttonBackgroundColor: UIColor {
        return _formIsValid ? #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1) : #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1).withAlphaComponent(0.5)
    }
}
