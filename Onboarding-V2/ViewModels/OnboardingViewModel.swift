//
//  OnboardingViewModel.swift
//  Onboarding-V2
//
//  Created by Jansen Ducusin on 4/7/21.
//

import UIKit

enum OnboardingViewModel: Int, CaseIterable {
    case metrics, dashboard, notification
    
    var title: String {
        switch self {
        case .metrics:
            return "What is Lorem Ipsum?"
        case .dashboard:
            return "Where does it come from?"
        case .notification:
            return "Why do we use it?"
        }
    }
    
    var description: String {
        switch self {
        case .metrics:
            return "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
        case .dashboard:
            return "It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old."
        case .notification:
            return "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout."
        }
    }
    
    var image: UIImage {
        switch self {
        case .metrics:
            return #imageLiteral(resourceName: "baseline_dashboard_white_48pt").withRenderingMode(.alwaysOriginal)
            
        case .dashboard:
            return #imageLiteral(resourceName: "baseline_insert_chart_white_48pt").withRenderingMode(.alwaysOriginal)
            
        case .notification:
            return #imageLiteral(resourceName: "baseline_notifications_active_white_48pt").withRenderingMode(.alwaysOriginal)
        }
    }
    
    var backgroundColor: UIColor {
        switch self {
        case .metrics:
            return .systemPurple
        case .dashboard:
            return .systemBlue
        case .notification:
            return .systemPink
        }
    }
    
    var shouldShow: Bool {
        switch self {
        case .notification:
            return true
        default:
            return false
        }
    }
    
}
