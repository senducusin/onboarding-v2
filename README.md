**Onboarding v2**

An improve version of my Onboarding (https://bitbucket.org/senducusin/onboarding/src/main/)

This app does not use storyboards except for the launch screen.  

---

## Features

- It has a login and registration screen
- Accounts are handled by Firebase's realtime database
- Login using a registered Gmail account thru the Google login button
- Added a welcome screen after the onboarding
- Forgot password mechanism

---

## Dependency

- Firebase: https://firebase.google.com/
- Paper-onboarding: https://github.com/ramotion/paper-onboarding
- JGProgressHUD: https://github.com/JonasGessner/JGProgressHUD

---

## Demo

![](/Previews/Preview.gif)